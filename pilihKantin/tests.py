from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from AddMakanan.models import Makanan
from AddMakanan.forms import MakananForm

class pilihKantin(TestCase):

    def test_app_exist(self):
        response = Client().get('/pilihKantin/')
        self.assertEqual(response.status_code, 200)

    def test_add_search(self):
        makanan = Makanan.objects.create(
            kantin = "Counter 1", 
            nama = "Ayam Goreng",
            harga = 10000,
            jenisMakanan = "Main Course",)
        self.assertTrue(isinstance(makanan, Makanan))
        self.assertEqual(makanan.nama, "Ayam Goreng")
        qs = Makanan.objects.all()
        title_contains_query="Ayam Goreng"
        qs = qs.filter(nama__icontains=title_contains_query)
        self.assertEqual(qs[0].nama , title_contains_query)
    
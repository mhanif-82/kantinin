Nama Aplikasi : Kantinin.id

Link Heroku : kantinin.herokuapp.com

Deskripsi:
Kantinin berupa sebuah website yang menyediakan daftar menu pada kantin sehingga orang-orang dapat menentukan makanan yang ingin dimakan sebelum membelinya

Manfaat:
- Pengguna dapat melihat daftar kantin, toko kantin, dan daftar makanan sekaligus dengan harga serta keterangan lain.
- Pengguna dapat mensorting makanan sesuai dengan keinginan
- Pengguna mendapat rekomendasi makanan

Fitur :
- Log in
- Pilih Kantin
- Category
- Sorting
- Feedback

Informasi Umum

Kelas : PPW D
Kelompok : KD 15
Kode AsDos : SMA

Anggota Kelompok :
Ali Irsyaad Nursyaban (1806141132)
Muhammad Hanif (1806186635)
Nabila Rizkiandini (1806191010)
Bob Fernando (1806235675)

[![pipeline status](https://gitlab.com/aliirsyaadn/kantinin/badges/master/pipeline.svg)](https://gitlab.com/aliirsyaadn/kantinin/commits/master)



from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Makanan
from .forms import MakananForm
from .views import addMakanan, success

class MakananUnitTest(TestCase):
    def test_add_makanan(self):
        self.assertFalse(Makanan.objects.all().exists())
        makanan = Makanan.objects.create(
            kantin = "Counter 1", 
            nama = "Ayam Goreng",
            harga = 10000,
            jenisMakanan = "Main Course",)
        self.assertTrue(isinstance(makanan, Makanan))
        self.assertTrue(Makanan.objects.filter(nama='Ayam Goreng').exists())
        self.assertEqual(makanan.nama, "Ayam Goreng")
    
    def test_add_makanan_using_index_func(self):
        found = resolve('/addmakanan/')
        self.assertEqual(found.func, addMakanan)

        found = resolve('/addmakanan/success')
        self.assertEqual(found.func, success)
    
    def test_form(self):
        form = MakananForm()
        self.assertFalse(form.is_valid())
    
    def test_app_exist(self):
        response = Client().get('/addmakanan/')
        self.assertEqual(response.status_code, 200)



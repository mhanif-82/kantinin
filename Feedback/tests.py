from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest

from .models import Feedback
from .forms import FeedbackForm

class FeedbackUnitTest(TestCase):
    def test_feedback(self):
        feedback = Feedback.objects.create(
            nama = "", 
            pesan = "")
        self.assertTrue(isinstance(feedback, Feedback))
        self.assertEqual(feedback.nama, "")
    
    def test_form(self):
        form = FeedbackForm()
        self.assertFalse(form.is_valid())
    
    def test_app_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 200)

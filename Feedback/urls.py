from django.urls import path
from . import views

app_name = 'Feedback'

urlpatterns = [
    path('', views.feedback, name='Feedback'),
    path('viewfeedback', views.viewfeedback, name='viewfeedback'),
]
